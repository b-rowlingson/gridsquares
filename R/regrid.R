## EWS has first grid char NOT I


# ews = grid[grid$order==1 & substr(grid$grid,1,1)!="I",]
# ews10k = ews[ews$resolution==10,]

## remake the grid geometry from the grid name.

## for some set of 10k grid squares eg ST23

## points(regrid10k(gridsq),col="red")

LLgrid100k <- function(sq, offsets=offsets100k()){
### e.g. "SW"
    s1 = offsets[substr(sq,1,1),,drop=FALSE]
    s2 = offsets[substr(sq,2,2),,drop=FALSE]
    s12 = (s1*5 + s2)
    rownames(s12) <- NULL
    s12[,1] = ((s12[,1]-2)*500000)
    s12[,2] = ((s12[,2]-1)*500000)
    colnames(s12)=c("x","y")
    s12
}


LLgrid50k <- function(sq50k, offs=offsets100k()){
### eg "SPNE"
    sq = substr(sq50k,1,2)
    rqs = LLgrid100k(sq, offs)
    off50 = rbind("SW"=c(0,0),"SE"=c(50000,0),"NW"=c(0,50000),"NE"=c(50000,50000))
    subsq = substr(sq50k,3,4)
    offs50k = off50[subsq,,drop=FALSE]
    rqs + offs50k
}

LLgrid20k <- function(sq20k, offs=offsets100k(), offs2=offsets20k()){
### eg "SP_G"
    sq = substr(sq20k,1,2)
    rqs = LLgrid100k(sq, offs)
    s20k = substr(sq20k,4,4)
    xoyo = offs2[s20k,,drop=FALSE]
    rqs + cbind(xoyo)
}

LLgrid10k <- function(sq4, offs=offsets100k()){
### eg "SP12"
    stopifnot(all(grepl("^[A-Z][A-Z][0-9][0-9]$",sq4)))
    sq = substr(sq4,1,2)
    xo = as.numeric(substr(sq4,3,3))
    yo = as.numeric(substr(sq4,4,4))
    rqs = LLgrid100k(sq, offs)
    rqs + cbind(xo*10000,yo*10000)
}


offsets100k <- function(){
    ## uses all letters except I
    grid_letters = LETTERS[-9]
    grid = matrix(grid_letters, 5,5, byrow=TRUE)
    LG = grid[5:1,]
    YG = replicate(5,(0:4)/5)
    XG = t(YG)
    d = cbind(XG=c(XG), YG=c(YG))
    row.names(d) = LG
    d
}

offsets20k <- function(){
    ## uses all letters except O
    grid_letters = LETTERS[-15]
    LG = matrix(grid_letters, 5, 5)
    YG = replicate(5, (0:4)/5)
    XG = t(YG)
    d = cbind(XG=100000*c(XG), YG=100000*c(YG))
    row.names(d) = LG
    d
    
}

testgrids <- function(){
    c(outer(c("S","T","N","H"), LETTERS[-9],paste0))
}

test10kgrid <- function(){
    sqs <- testgrids()
    d = expand.grid(sqs,0:9,0:9)
    paste0(d[,1],d[,2],d[,3])
}

all100kgrid <- function(top=c("S","T","N")){
    x2 = LETTERS[-9]
    d = expand.grid(top, x2)
    paste0(d[,1],d[,2])
}

all50kgrid <- function(){
    x1 = c("S","T","N")
    x2 = LETTERS[-9]
    x3 = c("NE","NW","SW","SE")
    d = expand.grid(x1,x2,x3)
    paste0(d[,1],d[,2],d[,3])
    
}

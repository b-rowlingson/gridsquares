st_rects <- function(pts, w, h){
    ptsm = st_coordinates(pts)
    lp = apply(ptsm,1, function(pt){st_matrix_rect(pt[1], pt[2], w,h)})

    
    sfc = do.call(st_sfc, lp)
    st_crs(sfc) = st_crs(pts)
    sfc
                         
   
}

st_matrix_rect <- function(x,y,w,h){
    xy = cbind(x+c(0,w,w,0,0),y+c(0,0,h,h,0))
    st_polygon(list(xy))
}

